package com.teleknesis.android.surveillance;
import com.enterprisedt.net.ftp.FTPConnectMode;
import com.enterprisedt.net.ftp.FileTransferClient;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import android.os.AsyncTask;
import android.util.Log;

import com.jcraft.jsch.SftpException;

/**
 * This still uses SSH to upload instead of FTP
 * 
 * 
 * */


public class UploadVideoThread extends AsyncTask<Void, Long, Boolean> {
	static String localFile;
	static String remoteFile;
	
    public UploadVideoThread(String inp, String out)  {
    	
    	localFile = new String(inp);
    	remoteFile = new String(out);
    	
    }

    
    // send file using ftp
    public void sendVideo(String fileName) {
		try {

			File testFile = new File(fileName);
			System.out.println(testFile.length());

			InputStream stream = new FileInputStream(testFile);
			OutputStream outputStream = VideoRecorder.ftpSocket.getOutputStream();

			int read = 0;
			byte[] buf = new byte[(int) testFile.length()];
			while ((read = stream.read(buf)) > 0) {
				outputStream.write(buf, 0, read);
				outputStream.flush();
			}
			
		} catch (IOException e) {
	
		}
    }
    
    @Override
    protected Boolean doInBackground(Void... params) {
    	// try uploading file
		try {
			VideoRecorder.sshCh.put(localFile,remoteFile);
			Log.i("upload", remoteFile);
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
    }
}
