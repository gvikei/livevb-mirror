package com.teleknesis.android.surveillance;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.logging.Logger;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.StrictMode;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FileTransferClient;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

//*******************************************************
//*******************************************************
// CamaraView
//*******************************************************
//*******************************************************
//@SuppressLint("NewApi")

public class VideoRecorder extends Activity implements OnClickListener,
	SurfaceHolder.Callback, OnInfoListener {
	
	static final String FTP_USER = "khuyen";
	static final String FTP_PASSWORD = "Android789";
	static final String FTP_HOST = "hansa.cs.okstate.edu";
	static final int FTP_PORT = 22;

	static JSch sshClient = new JSch();
	static Session session;
	static ChannelSftp sshCh;
	
	static Socket ftpSocket; 

	static MediaRecorder mRecorder;
	static SurfaceHolder mHolder;
	boolean mRecording = false;
	boolean mStop = false;
	boolean mPrepared = false;
	boolean mLoggedIn = false;
	private PowerManager.WakeLock mWakeLock;
	private static ArrayList<String> mFiles = new ArrayList<String>();
	private Timer mStopTimer;
	String remoteFilePath = "upload/";
	Button btnCapture;
	int loopCount = 0;

	static EditText txtSender;
	static EditText txtReceiver;
	static CheckBox chkSaveVideo;

	String lastFileUploaded;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		// ************ CREATE NEW MEDIA RECORDER OBJECT ****************
		// ************ Camera initialized & put on layout ***************
		mRecorder = new MediaRecorder();
		mRecorder.setOnInfoListener(this);
		initRecorder();
		setContentView(R.layout.camera);

		SurfaceView cameraView = (SurfaceView) findViewById(R.id.surface_camera);
		mHolder = cameraView.getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		// ******************** CREATE BUTTON CAPTURE **********************

		chkSaveVideo = (CheckBox) findViewById(R.id.chkSaveVideo);
		txtSender = (EditText) findViewById(R.id.txtSender);
		txtReceiver = (EditText) findViewById(R.id.txtReceiver);
		btnCapture = (Button) findViewById(R.id.btnCapture);
		btnCapture.setOnClickListener(this);

		// ******************************************************************

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "Ping");
		mWakeLock.acquire();

		
	}

	@Override
	protected void onDestroy() {
		try {
			super.onDestroy();
			mWakeLock.release();
		} catch (Exception e) {

		}
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	/*
	 * This method gets called every time new video is recorded			
	 */
	public static void initRecorder() { 		
		
		// set video profile
		mRecorder.setMaxDuration(2000);
		mRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
		mRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		CamcorderProfile profile = CamcorderProfile
				.get(CamcorderProfile.QUALITY_LOW); // this can be customized to enhance video quality
		mRecorder.setProfile(profile);

		// check & create video directory
		File mediaStorageDir = new File("/sdcard/LiveVB/");
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// Logger.Debug("failed to create directory");
			}
		}

		// set each video with a new file name
		String senderID = Home.txtSender.getText().toString();
		String receiverID = Home.txtReceiver.getText().toString();
		String filePath = "/sdcard/Surveillance/" + "VID_" + senderID + "_"
				+ receiverID + "_"
				+ new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

		if (Home.chkSaveVideo.isChecked()) {
			filePath = filePath + "_s"; // _s shows that video should be saved
		}

		filePath = filePath + ".mp4";
		mFiles.add(filePath);
		mRecorder.setOutputFile(filePath);

	}

	private void prepareRecorder() {
		// Logger.Debug( "About to prepare" );
		mRecorder.setPreviewDisplay(mHolder.getSurface());

		try {
			mRecorder.prepare();
			mPrepared = true;
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onClick(View v) {

		try {
			if (mRecording) {

				mRecorder.stop();
				mStopTimer.cancel();
				mPrepared = false;
				mRecording = false;

				String lastLocalFile = new File(mFiles.get(mFiles.size() - 2))
						.getName();
				do {
					restartRecorder();
				} while (lastLocalFile != lastFileUploaded);

				btnCapture.setText("Start");
				// session.disconnect();

				mRecorder.reset();
				this.initRecorder();
				this.prepareRecorder();

			} else {

				try {
				/*
					// set up new connection using FTP
					ftpSocket = new Socket(FTP_HOST, 9999); 
				*/
					
					session = sshClient
							.getSession(FTP_USER, FTP_HOST, FTP_PORT);
							
					java.util.Properties config = new java.util.Properties();
					config.put("StrictHostKeyChecking", "no");
					session.setConfig(config);
					session.setPassword(FTP_PASSWORD);
							
					session.connect();
							
				} catch (Exception e) {
					e.printStackTrace();
				}

			
				// part 2
				Channel channel;
				try {
					channel = session.openChannel("sftp");
					channel.connect();
					sshCh = (ChannelSftp) channel;
					Log.i("upload", "channel-p2 done");
				} catch (JSchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				btnCapture.setText("Stop");
				mRecorder.start();
				mRecording = true;
			}
		}

		catch (Exception e) {
			// Logger.Error( "onClick", e );
		}
	}

	public void restartRecorder() {
		loopCount++;

		/*
		 * //Logger.Debug("'bout to restart recorder");
		 * btnCapture.setText("Start"); mRecorder.reset(); mPrepared = false;
		 * mRecording = false;
		 * 
		 * initRecorder(); prepareRecorder(); mRecorder.start(); mRecording =
		 * true; btnCapture.setText("Stop");
		 */
		
		mRecording = true;
		if (mFiles.size() >= 2) {
			
			// Start uploading the last file
			File newFile = new File(mFiles.get(mFiles.size() - 2));

			String remoteFile = remoteFilePath + newFile.getName();

			try {
				// upload file
				new UploadVideoThread(
						newFile.getAbsolutePath(), remoteFile).execute();
				
				lastFileUploaded = remoteFile;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void surfaceCreated(SurfaceHolder holder) {
		if (!mPrepared) {
			prepareRecorder();
		}
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		mRecorder.release();

		/*
		 * if (mRecording) {
		 * 
		 * mStopTimer.cancel(); mRecorder.stop(); mRecording = false;
		 * 
		 * String lastLocalFile = new File
		 * (mFiles.get(mFiles.size()-2)).getName(); do { //btnCapture.setText(
		 * "Uploading file - Please don't start new record yet");
		 * //Toast.makeText(getApplicationContext(), "Stopclicked",
		 * Toast.LENGTH_SHORT).show(); restartRecorder(); } while (lastLocalFile
		 * != lastFileUploaded); //mRecorder.release(); }
		 * 
		 * // done uploading file
		 */
	}

	public void onInfo(MediaRecorder arg0, int arg1, int arg2) {
		if (arg1 == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
			restartRecorder();
			mRecorder.reset();
			initRecorder();
			new CreateVid(mRecorder, mFiles.get(mFiles.size() - 2)).execute();
		}
	}

}