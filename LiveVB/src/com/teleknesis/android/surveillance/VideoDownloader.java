package com.teleknesis.android.surveillance;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Timer;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnInfoListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StrictMode;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.VideoView;

public class VideoDownloader extends Activity implements OnClickListener,
		SurfaceHolder.Callback, OnInfoListener {
	static final String FTP_USER = "khuyen";
	static final String FTP_PASSWORD = "Android789";
	static final String FTP_HOST = "hansa.cs.okstate.edu";
	static final int FTP_PORT = 9999;

	static com.teleknesis.android.surveillance.VideoView cameraView;
	static SurfaceHolder mHolder;
	static ProgressBar mDialog;
	static EditText txtAllFiles;
	static MediaPlayer mPlayer;

	boolean mRecording = false;
	boolean mStop = false;
	boolean mPrepared = false;
	boolean mLoggedIn = false;
	private PowerManager.WakeLock mWakeLock;
	private static ArrayList<String> mFiles = new ArrayList<String>();
	private Timer mStopTimer;
	String remoteFilePath = "upload/";
	String localFilePath = "/sdcard/Surveillance/download/";
	Button btnPlay;
	int loopCount = 0;
	static int currentSong = 0;
	static ArrayList<String> filesToReceive = new ArrayList<String>();;
	static ArrayList<String> filesToDownload = new ArrayList<String>();
	static ArrayList<String> filesForReceiver = new ArrayList<String>();
	static ArrayList<String> playedFiles = new ArrayList<String>();
	static AlertDialog alert;

	static EditText txtSender;
	static EditText txtReceiver;

	String lastFileUploaded;
	static String localSenderID;
	static String serverSender;
	static String localReceiverID;

	boolean canSaveVideo;
	boolean connectedToServer = false;
	//Test2 qq;
	static SocketObj socket;
	static OutputStream outToServer;
	static PrintWriter pw;
	static InputStream inFromServer;
	static BufferedReader br;

	boolean newFileFound;

	static String VIDEO_FILE_FOR_TESTING = null;

	public VideoDownloader() {
		if (socket == null) {
			try {
				socket = (SocketObj) new SocketObj(FTP_HOST, FTP_PORT);
			} catch (UnknownHostException e) {
				connectedToServer = false;
				e.printStackTrace();

			} catch (IOException e) {
				connectedToServer = false;
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		// ************ CREATE NEW MEDIA RECORDER OBJECT ****************
		
		// ************ Camera initialized & put on layout ***************
		setContentView(R.layout.download);

		cameraView = (com.teleknesis.android.surveillance.VideoView) findViewById(R.id.videoView1);

		mHolder = cameraView.getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		// ******************** CREATE BUTTON PLAY **********************
		btnPlay = (Button) findViewById(R.id.buttonPlay);
		btnPlay.setOnClickListener(this);
		txtAllFiles = (EditText) findViewById(R.id.txtAllFiles);

		// ******************************************************************

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "Ping");
		mWakeLock.acquire();

	}

	/*
	 * @Override public void onResume() { super.onResume();
	 * 
	 * 
	 * }
	 */

	public void onStart() {
		super.onStart();
		initDownloader();
	}

	public void initDownloader() {
		// set auto download for video view
		cameraView.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				try {
					System.out.println("finish once");
					// checkFilesFromServer();
					
					//if (newFileFound) {
					System.out.println(currentSong + " " + filesToReceive.size());
					
					if (currentSong < filesToReceive.size()) {
						autoStreaming2();
						
					}
	
					/*
					 * Log.i("stream", "currentSong" + currentSong); if
					 * (currentSong + 1 < filesToDownload.size()) {
					 * currentSong++; getFilesFromServer();
					 * cameraView.start(); cameraView.requestFocus(); }
					 */

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}/*
				 * catch (InterruptedException e) { // TODO Auto-generated
				 * catch block e.printStackTrace(); }
				 */catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		// only download video from this sender
		localSenderID = Home.wantedSenderID;

		// auto streaming
		try {
			// checkFilesFromServer();
			getFilesFromServer();
			autoStreaming2();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void onInfo(MediaRecorder arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		// TODO Auto-generated method stub

	}

	public boolean connectToServer() {

		try {
			if (socket == null)
				socket = (SocketObj) new SocketObj(FTP_HOST, FTP_PORT);
			/*
			 * inFromServer = socket.getInputStream(); br = new
			 * BufferedReader(new InputStreamReader(inFromServer)); outToServer
			 * = socket.getOutputStream(); pw = new PrintWriter(outToServer);
			 */
		} catch (UnknownHostException e) {
			connectedToServer = false;
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			connectedToServer = false;
			e.printStackTrace();
			return false;
		}

		try {
			inFromServer = socket.getInputStream();
			br = new BufferedReader(new InputStreamReader(inFromServer));
			System.out.println("Server says " + br.readLine()); // just to wait
																// for server to
																// say it is
																// ready
			outToServer = socket.getOutputStream();
			pw = new PrintWriter(outToServer);
			connectedToServer = true;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			connectedToServer = false;
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public void checkFilesFromServer() throws IOException {
		// connect to server
		boolean connected = connectToServer();

		newFileFound = false;

		// get videos according to receiver's username
		if (connected) {
			// String localReceiverID = Home.txtSender.getText().toString();
			// //txtSender acts as receiver's name in this case
			String localReceiverID = Home.myID;
			Log.i("getfiles", localReceiverID);

			/*
			 * // create a list of files for this receiver from both wanted &
			 * unwanted senders filesForReceiver = new ArrayList<String>();
			 */
			outToServer = socket.getOutputStream();
			pw = new PrintWriter(outToServer);
			
			// tell server the receiver's ID
			pw.println(localReceiverID);
			pw.flush();
			System.out.println("Sending " + localReceiverID);

			// check for new file from server
			String inText = "";
			while ((inText = br.readLine()) != null) {
				if (inText != null) {
					
					// if the localReceiverID matches with any ReceiverID on Server,
					// Server will announce this Receiver has new file to receive
					this.serverSender = inText; 
					this.newFileFound = true;
				}
				break;
			}

			/*
			 * //System.out.println(getDataSource(inFromServer));
			 * inFromServer.close(); socket.close();
			 */


		}

		else {
			Log.i("error", "Could not connect to server");
		}
	}

	/*
	 * public void downloadVideosFromServer() { File mediaStorageDir = new
	 * File(localFilePath); if ( !mediaStorageDir.exists() ) { if (
	 * !mediaStorageDir.mkdirs() ){
	 * //Logger.Debug("failed to create directory"); } }
	 * 
	 * boolean connected = sshCh.isConnected();
	 * 
	 * 
	 * if (connected) { try { for (String fileName : filesToDownload) {
	 * DownloadFile.mFileLen = filesToDownload.size(); new
	 * DownloadFile(fileName, localFilePath + fileName);
	 * 
	 * txtAllFiles.setText(txtAllFiles.getText() + "\n" + localFilePath +
	 * fileName); }
	 * 
	 * } catch (SftpException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 * 
	 * }
	 */

	public void getFilesFromServer() throws IOException, InterruptedException {

		String inText = "";
		String fileName = "";

		String videoPath = null;
		/*
		 * inFromServer = socket.getInputStream(); br = new BufferedReader(new
		 * InputStreamReader(inFromServer)); outToServer =
		 * socket.getOutputStream(); pw = new PrintWriter(outToServer);
		 */

			getList: 
			while ((playedFiles.size() == 0) && ((inText = br.readLine()) != null)) {
			

			if (inText != null) {
				fileName = inText;

				// print out file name
				System.out.println("fileName from server: " + fileName);

				// get which files to receive until receiving "DONE" signal
				if (!fileName.equals("DONE")) {

					// if file has not been played
					if (playedFiles.contains(fileName) == false) {

						// tell server to get this file
						pw.println(fileName);
						pw.flush();

						// get a list of file to receive from server
						filesToReceive.add(fileName);
					}
				} else {
					System.out.println("about to break");
					break getList;
				}
				//break;
			}
		}

	}


	public void autoStreaming2() throws IOException, InterruptedException {
		Log.i("localSenderID", "" + localSenderID
				+ " files for receiver size: " + filesForReceiver.size());
		
		
		System.out.println("currentSong: " + currentSong + " " + filesToReceive.size());
		// start downloading
		
		
		if (currentSong < filesToReceive.size()) {
			// for (String file : filesToReceive)
			String file = filesToReceive.get(currentSong);

			// print out file name
			System.out.println("going to download this file: " + file);

			// enable saving video mode
			if (file.contains("_s.mp4")) {
				canSaveVideo = true;
			} else {
				canSaveVideo = false;
			}

			// download video
			String videoPath = null;
			try {
				videoPath = getDataSource(inFromServer);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.err.println("error getting data source from server");
				e.printStackTrace();
			}

			System.out.println("videoPath: " + videoPath);
			
			// put this into 'played' list
			playedFiles.add(file);
			
			System.out.println("in autostreaming2: " + file);
			
			currentSong++;
			// play video
			playVideo2(videoPath);
			 
		}

		// checkFilesFromServer();

		// getFilesFromServer();

		// playVideo2();

		/*
		 * // get list of files from WANTED receiver only for (String
		 * file:filesForReceiver) {
		 * 
		 * Log.i("file in files for receiver", file); String remoteSenderID =
		 * getSenderID(file);
		 * 
		 * if (remoteSenderID.equals(localSenderID)) {
		 * 
		 * 
		 * if (!filesToDownload.contains(file) && (!new File (localFilePath +
		 * file).exists())) { filesToDownload.add(file);
		 * Log.i("filesToDownload.size()",
		 * Integer.toString(filesToDownload.size())); } else {
		 * Log.i("auto streaming", "file already exists: " + file); }
		 * 
		 * } else { Log.i("auto streaming", "remoteSenderID <> localSenderID" +
		 * " " + remoteSenderID + " " + localSenderID); }
		 */

		/*
		 * if (filesToDownload.size() > 0) { getFilesFromServer(); }
		 */

	}

	private void playVideo2(String videoPath) {

		// for debugging
		System.out.println("playing videos");
		System.out.println(videoPath);

		
		// set video path
		
		cameraView.setVideoURI(Uri.parse(VideoProvider.CONTENT_URI_BASE
				+ Uri.encode(videoPath)));

		try {
			cameraView.start();
			cameraView.requestFocus();

		} catch (Exception e) {
			Log.e("tag", "error: " + e.getMessage(), e);
			if (cameraView != null) {
				cameraView.stopPlayback();
			}
		}
	}

	/**
	 * InputStream stream is passed from socket's inputStream
	 * 
	 */
	private String getDataSource(InputStream stream) throws IOException {
		if (stream == null)
			throw new RuntimeException("stream is null");

		File temp;

		if (!canSaveVideo) {
			temp = File.createTempFile("mediaplayertmp", ".mp4");
			temp.deleteOnExit();
		} else {
			File dir = new File(localFilePath);
			if (!dir.exists()) {
				dir.mkdirs();
			}

			temp = new File(
					localFilePath
							+ "VID_"
							+ localSenderID
							+ "_"
							+ new SimpleDateFormat("yyyyMMdd_HHmmss")
									.format(new Date()) + ".mp4");
		}

		String tempPath = temp.getAbsolutePath();
		FileOutputStream out = new FileOutputStream(temp);

		int read = 0;
		long total = 0;
		byte buf[]; //= new byte[128000];
		
		/*
		
		long fileSize = Long.parseLong(br.readLine());
		System.out.println("fileSize: " + fileSize);
		buf = new byte[(int) fileSize];
		
		*/
		
		/*
		while ((read = stream.read(buf, 0, (int) fileSize)) >= 0) {
			System.out.print(read + " ");
			if (read < 0) break;
			total += read;
			out.write(buf, 0, read);
			out.flush();
			if (total == fileSize){
				System.out.println("equals to fileSize");
				break;
			}
		}
		System.out.print("\n"+read + " ");
		*/
		
		/*
		
		do {
			read = stream.read(buf, 0, (int) fileSize);
			System.out.print(read + " ");
			if (read < 0) break;
			out.write(buf, 0, read);
			out.flush();
		} while (true);
		
		out.close();

		*/
		
		//BufferedInputStream bis = new BufferedInputStream(stream);
		DataInputStream bis = new DataInputStream(stream);
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		
		int length = bis.read();
		
		while ((length != -1) && (length != '\n')) {
			buffer.write(length);
			length = bis.read();
		}
		
		if (length == -1) {
			System.err.print("unexpected end of stream");
			return null;
		}
		
		String size = buffer.toString("UTF-8");
		System.out.println("size" + size + " bytes read: " + buffer.size());
		buffer.reset();
		
		// read the raw data
		byte[] outBuffer = new byte[Integer.valueOf(size)];//new byte[socket.getReceiveBufferSize()];
		int bytesReceived = 0;
		int totalBytesReceived = 0;
		
		/*
		
		while (true) {
			// read bytes from stream to outBuffer
			//***** bytesReceived = bis.read(outBuffer);
			bis.readFully(outBuffer);
			
			// for debugging
			totalBytesReceived += bytesReceived;
			System.out.println(totalBytesReceived + " == " + Integer.valueOf(size) + " " + (totalBytesReceived == Integer.valueOf(size)));
			
			// if no bytes were read, exit
			if ((bytesReceived <= 0) || 
					(buffer.size() == Integer.valueOf(size))) break;
			
			byte[] copyBuffer = outBuffer.clone();
			out.write(copyBuffer, 0, bytesReceived);
			out.flush();
			
			
			// write out num read + total read
			System.out.println(bytesReceived + " " + buffer.size());
			
			// write from outBuffer to buffer
			buffer.write(outBuffer, 0, bytesReceived);
			
			// create new outBuffer for reading
			outBuffer = new byte[socket.getReceiveBufferSize()];
			
		}
		*/
		bis.readFully(outBuffer);
		out.write(outBuffer);
		out.flush();
		
		System.out.println("len: "+outBuffer.length);
		out.close();
		
		VIDEO_FILE_FOR_TESTING = tempPath;
		return tempPath;
	}

	public void onClick(View arg0) {

		/*
		 * try { autoStreaming2(); } catch (IOException e) {
		 * e.printStackTrace(); } catch (InterruptedException e) {
		 * e.printStackTrace(); }
		 */

		// cameraView.setVideoPath("//sdcard/Surveillance/download/VID_KDuong_20140414_125244.mp4");
		// playVideo2("//sdcard/Surveillance/download/VID_KDuong_20140414_125244.mp4");
		/*
		System.out.println("VIDEO_FILE_FOR_TESTING: " + VIDEO_FILE_FOR_TESTING);
		playVideo2(VIDEO_FILE_FOR_TESTING);
		*/
		
		// reset
		currentSong = 0;
		playedFiles = new ArrayList<String>();
		
		// reload
		
		try {
			
			checkFilesFromServer();
			getFilesFromServer();
			autoStreaming2();
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}

	/**
	 * OnCompletionListener completionListener=new OnCompletionListener() {
	 * public void onCompletion(MediaPlayer mp) {
	 * 
	 * Log.i("blah", Integer.toString(currentSong));
	 * 
	 * File vidDir = new File(localFilePath); if ( !vidDir.exists() ) { if (
	 * !vidDir.mkdirs() ){ Logger.Debug("failed to create directory"); } }
	 * File[] vidFiles = vidDir.listFiles();
	 * System.out.println(vidFiles.length);
	 * 
	 * if (currentSong == vidFiles.length) { mp.release(); return; }
	 * 
	 * VideoPlayerSetup(mPlayer,vidFiles[currentSong].getPath());
	 * mPlayer.prepareAsync(); currentSong++; };
	 * 
	 * };
	 * 
	 * public void VideoPlayerSetup(MediaPlayer mPlayer, String dataSource) {
	 * try { Log.i("blah", dataSource); mPlayer.reset();
	 * mPlayer.setDataSource(dataSource); } catch (IllegalArgumentException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (IllegalStateException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } try { //mPlayer.prepare(); } catch
	 * (IllegalStateException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } txtAllFiles.setText(dataSource); }
	 * 
	 * OnPreparedListener preparedListener = new OnPreparedListener() { public
	 * void onPrepared(MediaPlayer mp) { // TODO Auto-generated method stub
	 * mp.start(); } };
	 * 
	 * public void playVideos() { mPlayer = new MediaPlayer();
	 * mPlayer.setDisplay(mHolder);
	 * mPlayer.setOnCompletionListener(completionListener);
	 * mPlayer.setOnPreparedListener(preparedListener);
	 * 
	 * txtAllFiles.setText(""); currentSong = 0;
	 * 
	 * completionListener.onCompletion(mPlayer); }
	 * 
	 * public void TestPlayingVideo(View v) { playVideos(); }
	 */

	public String getSenderID(String filename) {
		int _1stPos = 0;
		int _2ndPos = 0;
		for (int i = 0; i < filename.length(); i++) {
			if (filename.charAt(i) == '_') {
				_1stPos = i;
				for (int j = i + 1; j < filename.length(); j++) {
					if (filename.charAt(j) == '_') {
						_2ndPos = j;
						break;
					}
				}
				if (_1stPos != 0)
					break;
			}
		}
		return (filename.substring(_1stPos + 1, _2ndPos));
	}

	public String getReceiverID(String filename) {
		int startPos = filename.indexOf(getSenderID(filename));

		int _1stPos = 0;
		int _2ndPos = 0;

		for (int i = startPos; i < filename.length(); i++) {
			if (filename.charAt(i) == '_') {
				_1stPos = i;
				for (int j = i + 1; j < filename.length(); j++) {
					if (filename.charAt(j) == '_') {
						_2ndPos = j;
						break;
					}
				}
				if (_1stPos != 0)
					break;
			}
		}
		return (filename.substring(_1stPos + 1, _2ndPos));
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (alert != null) {
			alert.dismiss();
			alert = null;
		}
	}
}