package com.teleknesis.android.surveillance;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketObj extends Socket implements Serializable {
	public SocketObj () {
		super();
	}

	public SocketObj(String ftpHost, int ftpPort) throws UnknownHostException, IOException {
		super(ftpHost, ftpPort);
	}
}
