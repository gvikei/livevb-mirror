package com.teleknesis.android.surveillance;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;


public class Home extends Activity {
	static CheckBox chkSaveVideo;
	static EditText txtSender;
	static EditText txtReceiver;
	public static String wantedSenderID;
	public static String myID;
	private MediaRecorder mRecorder;
	private Handler handler;
	private AlertDialog alert;
	private boolean alertShowed = false;
	public static SocketObj socket;
	private VideoDownloader vd = new VideoDownloader();
	
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
        	
        	// set up layout
			super.onCreate(savedInstanceState);
			setContentView(R.layout.main);
			mRecorder = new MediaRecorder();
			txtSender = (EditText) findViewById(R.id.txtSender);
			txtReceiver = (EditText) findViewById(R.id.txtReceiver);
			chkSaveVideo = (CheckBox) findViewById(R.id.chkSaveVideo);

			// for testing allowing to save video
			chkSaveVideo.setChecked(true);

			} 
        catch (Exception error) {
			//Log.Error( "Home_onCreate", error );
			}
    	}
    
    public void getStarted(View v) throws IOException, InterruptedException {
    	myID = txtSender.getText().toString();
    	
    	/*
		 * start timer for checking new vids
		 * 
		 
    		handler = new Handler();
			handler.postDelayed(runnable, 100);
			
		 */
    	
    	// prepare for notification
    	checkNewVideos();
		
    	// get socket
    	socket = vd.socket;
		
    	// for debugging
		System.out.println("socket stream is null? " + (socket.getInputStream() == null));
    }
    
    
    public void LaunchCameraActivity( View v ) {
    	startActivityForResult( new Intent(Home.this, VideoRecorder.class), 1000);
    }       
    
    @SuppressWarnings("static-access")
	public void LaunchDownloader (View v) throws IOException, InterruptedException {
    	//checkNewVideos();
    	//startActivityForResult(new Intent(Home.this, VideoDownloader.class), 1000);
    }
    

    private Runnable runnable = new Runnable() {
       public void run() {
         
    	   
          try {
			checkNewVideos();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
          
     	  //Toast.makeText(getApplicationContext(),""+ System.currentTimeMillis(), Toast.LENGTH_LONG).show();
          /* and here comes the "trick" */
          handler.postDelayed(this, 1000);
       }
    };
    
    @SuppressWarnings("static-access")
	public void checkNewVideos() throws IOException, InterruptedException {
    	
    	Log.i("check new videos", "called");
    	
    	/*
    	if (vd.connectedToServer == false) {
    		vd.checkFilesFromServer(); // only call this if timer is disabled
    		vd.initDownloader(); // this call autostreaming2
    		while (vd.filesForReceiver.size() < 1) {
    				vd.checkFilesFromServer();
    				Thread.sleep(500);
    		}
    	}
    	
    	if (vd.filesForReceiver.size() > 0) {
			if (!alertShowed) {
				showAlert();
			}
		}
		*/
    	
    	//vd.checkFilesFromServer();
    	
    	// if newFileFound <=> checkFilesFromServer was called before
    	if (vd.newFileFound == true) {
    		
    		// for debugging
    		System.out.println("new file found");
    		
    		// check if alert has not been showed
    		if (!alertShowed) {
    			Log.i("alert", "alert show called");
    			showAlert();
    		
    		// do not show alert anymore
    		} else {
    			System.out.println("in home, check new file, alert showed");
    		}
    		
    	// call vd.checkFilesFromServer for the first time 
    	} else {
    		vd.checkFilesFromServer();
    	}
    }
    
    public void showAlert() {
    	
    	// set up alert dialog
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setTitle("New video");
	    
	    // get the name of the newest sender
	    final String senderFromServer = vd.serverSender;
	    builder.setMessage("Do you want to watch new video from " + senderFromServer + " ?");
	    
	    // Yes clicked
	    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	        	
	        	// receiver's accepted sender ID
	        	wantedSenderID = senderFromServer;
	        	
	        	// for debugging
	        	Log.i("wantedSenderID", "" + wantedSenderID);
	        	dialog.dismiss();
	        	
	    //handler.removeCallbacks(runnable);
	        	
	        	// show the 'Play video' activity with the current socket
	        	Intent i = new Intent(Home.this, VideoDownloader.class);
	        	i.putExtra("sk", socket);
	        	startActivity(i);
	        }

	    });

	    // NO clicked
	    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) {
	            // Do nothing
	            dialog.dismiss();
	        }
	    });

	    
	    alert = builder.create();
	    alert.show();
	    alertShowed = true;
	    
    }
    
    public void onStop() {
    	 super.onStop();
    	 if (alert != null) {
    	  alert.dismiss();
    	  alert = null;
    	 }
    	 alertShowed = false;
    	
    //handler.removeCallbacks(runnable);
	}
    
    public void onPause() {
    	super.onPause();
    	//handler.removeCallbacks(runnable);
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	//handler.removeCallbacks(runnable);
    }
    
    public void onResume() {
    	// start timer for checking new vids
    	super.onResume();
    	//handler.postDelayed(runnable, 1000);
    }
}