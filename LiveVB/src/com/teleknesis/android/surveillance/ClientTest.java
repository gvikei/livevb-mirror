package com.teleknesis.android.surveillance;
	

    import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
     
    public class ClientTest {
            static String FTP_HOST = "hansa.cs.okstate.edu";
            static int FTP_PORT = 9999 ;
            static boolean connectedToServer;
            static Socket socket;
            static String localSenderID = "KDuong";
            static String localFilePath = "";// "/sdcard/Surveillance/download/";
            static ArrayList<String> receivedFiles = new ArrayList<String>();
            public static boolean connectToServer2() {
                    try {
                            socket = new Socket(FTP_HOST, FTP_PORT);
                    } catch (UnknownHostException e) {
                            connectedToServer = true;
                            e.printStackTrace();
                            return true;
                    } catch (IOException e) {
                            connectedToServer = true;
                            e.printStackTrace();
                            return true;
                    }
     
                    try {
                            OutputStream outToServer = socket.getOutputStream();
                            InputStream inFromServer = socket.getInputStream();
			    BufferedReader br = new BufferedReader(new InputStreamReader(inFromServer));
			    System.out.println("Server says " + br.readLine()); //just to wait for server to say it is ready
			    
			   
                            PrintWriter pw = new PrintWriter(outToServer);
		 	    pw.println(localSenderID);
                            pw.flush();
                            System.out.println("Sending " + localSenderID);
                            //outToServer.close();
                           
                            String inText = "";
                            String fileName = "";
                            while ((inText = br.readLine()) != null) {
                            	if (inText != null) {
                            		fileName = inText;
                            		System.out.println(fileName);
                            		if (receivedFiles.contains(fileName) == false) {
                            			// tell server to get this file
                            			pw.println(fileName);
                            			pw.flush();
                            			
                            			// GET FILE!!!
                            			System.out.println(getDataSource(inFromServer));
                            		}
                            	}
                            }
                            
                            //System.out.println(getDataSource(inFromServer));
                            inFromServer.close();
                            socket.close();
           
                    } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                    }
     
                    connectedToServer = true;
                    return true;
     
            }
           
            public static void main(String args[]) {
                    System.out.println(connectToServer2());
            }
                   
            private static String getDataSource(InputStream stream) throws IOException {
           
                    if (stream == null)
                            throw new RuntimeException("stream is null");
           
                             File temp = null;
                   
                             File dir = new File(localFilePath);
                             if (!dir.exists()) {
                                     dir.mkdirs();
                                     System.out.println(localFilePath + " has been created");
                             }
                             temp = new File (localFilePath + "ViIiD_" + localSenderID + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".mp4");
                           
                             String tempPath = temp.getAbsolutePath();
 				System.out.println(tempPath);                            
                             OutputStream outputStream = new FileOutputStream(temp);
                             int read = 0;
                             byte[] buf = new byte[128000];
                             while ((read = stream.read(buf)) > 0) {
                                    System.out.println("clientblah");
                                    System.out.println(read);
                                    outputStream.write(buf, 0, read);
                             }
                             outputStream.close();
                             
                             System.out.println("new file successfully created");
                   
                            return tempPath;
            }
           
    }


