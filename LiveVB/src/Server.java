

import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;

import java.io.*;

public class Server {
	static ServerSocket serverSocket = null;
	
	public static void startNewConnection() throws IOException {
		while (true) {
			Socket socket = serverSocket.accept();
			System.err.println("Started new connection");
			SendVideoThread sendThread = new SendVideoThread(socket);
			sendThread.start();
		}
	}

	public static void main(String[] args) throws IOException {
		try {
			serverSocket = new ServerSocket(9999);
		} catch (IOException e) {
			System.err.println("Could not listen on port: 9999.");
			System.exit(-1);
		}

		System.err.println("Starting");

		/*
		 * Socket s2 = serverSocket.accept();
		 * System.err.println("Started new connection"); ReceiveVideoThread st2
		 * = new ReceiveVideoThread(s2); st2.start();
		 */

			startNewConnection();
	}

	public static String getSenderID(String filename) {
		int _1stPos = 0;
		int _2ndPos = 0;
		for (int i = 0; i < filename.length(); i++) {
			if (filename.charAt(i) == '_') {
				_1stPos = i;
				for (int j = i + 1; j < filename.length(); j++) {
					if (filename.charAt(j) == '_') {
						_2ndPos = j;
						break;
					}
				}
				if (_1stPos != 0)
					break;
			}
		}
		return (filename.substring(_1stPos + 1, _2ndPos));
	}

	public static String[] getReceiverID(String filename) {
		int startPos = filename.indexOf(getSenderID(filename));

		int _1stPos = 0;
		int _2ndPos = 0;

		for (int i = startPos; i < filename.length(); i++) {
			if (filename.charAt(i) == '_') {
				_1stPos = i;
				for (int j = i + 1; j < filename.length(); j++) {
					if (filename.charAt(j) == '_') {
						_2ndPos = j;
						break;
					}
				}
				if (_1stPos != 0)
					break;
			}
		}
		
		String allReceiverIDs = filename.substring(_1stPos + 1, _2ndPos);
		return (allReceiverIDs.split(","));
	}

}

class SendVideoThread extends Thread {
	private Socket socket = null;
	String remoteFilePath = "upload/";

	public SendVideoThread(Socket socket) {
		super("SendVideoThread");
		this.socket = socket;
	}

	public void sendVideo(String fileName) {
		try {

			File testFile = new File(remoteFilePath + fileName);
			System.out.println(testFile.length());

			InputStream stream = new FileInputStream(testFile);
			OutputStream outputStream = socket.getOutputStream();
			// OutputStream outputStream = new FileOutputStream(new File
			// ("abc.mp4"));
			PrintWriter pw = new PrintWriter(outputStream);
			pw.print(testFile.length() + "\n");
			pw.flush();

			int read = 0;
			byte[] buf = new byte[(int) testFile.length()];
			while ((read = stream.read(buf)) > 0) {
				outputStream.write(buf, 0, read);
				outputStream.flush();
			}
			// outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void run() {
		System.out.println("Started new thread");
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(socket.getOutputStream());
			pw.println("READY");
			pw.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}

		ArrayList<String> filesForReceiver = new ArrayList<String>();
		ArrayList<String> filesToSend = new ArrayList<String>();
		InputStream inFromClient = null;

		try {
			inFromClient = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// get user name
		BufferedReader br = new BufferedReader(new InputStreamReader(
				inFromClient));
		String inText = "";
		String localReceiverID = "";
		System.out.println("Getting username");

		try {
			while ((inText = br.readLine()) != null) {
				if (inText != null)
					localReceiverID = inText;
				System.out.println(localReceiverID);
				break; // once we get username, get out of loop
			}
			System.out.println("Got username");

			// get all videos files
			File mp4Files = new File(remoteFilePath);
			File[] allFiles = mp4Files.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".mp4");
				}
			});

			// get list of files send to the this receiver
			for (File entry : allFiles) {
				// get the receiver's name from server files
				String[] remoteReceiverIDs = Server.getReceiverID(entry.getName());
				
				
				
				// print out for debugging
				for (String remoteReceiverID : remoteReceiverIDs) {
					System.out.println("remote ID: "
							+ remoteReceiverID
							+ " localID: "
							+ localReceiverID
							+ " equals = "
							+ Boolean.toString(remoteReceiverID
									.equals(localReceiverID)));
				}
								
				

				// get only the files that have local receiver's ID
				if (Arrays.asList(remoteReceiverIDs).contains((localReceiverID))) {
					filesForReceiver.add(entry.getName());
				}
			}

			// tell user if new files found
			ArrayList<String> senderList = new ArrayList<String>();

			if (filesForReceiver.size() > 0) {
				for (String file : filesForReceiver) {
					String sender = Server.getSenderID(file);
					if (!senderList.contains(sender)) {
						System.out.println("sender: " + sender);
						pw.println(sender);
						pw.flush();
						senderList.add(sender);
					}
				}
			}

			// get which files user want to receive
			for (String file : filesForReceiver) {
				// send file name to user
				pw.println(file);
				pw.flush();

				// see if user agrees to get that file
				inText = "";
				String fileName = "";
				while ((inText = br.readLine()) != null) {
					if (inText != null) {
						fileName = inText;

						// if the user returns the same file name, meaning
						// wanting to receive that file
						System.out.println(fileName);
						if (fileName.equals(file)) {
							// tell server to get this file
							System.out.println("going to get this file: "
									+ fileName);
							filesToSend.add(fileName);
						} else {
							System.out.println("file name doesn't match: "
									+ fileName + " || " + file);
						}
					}
					break;
				}
			}

			// tell client that server is done sending list of files
			pw.println("DONE");
			pw.flush();

			// send files
			for (String file : filesToSend) {
				System.out.println(filesToSend.size());
				sendVideo(file);
				/*
				 * pw.println("done"); pw.flush();
				 */
			}
			OutputStream outputStream = socket.getOutputStream();
			outputStream.close();
			Server.startNewConnection();
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

class ReceiveVideoThread extends Thread {
	private Socket socket = null;

	public ReceiveVideoThread(Socket socket) {
		super("ReceiveVideoThread");
		this.socket = socket;
	}

	public void run() {

		try {
			for (int i = 0; i < 2; i++) {
				OutputStream outputStream = socket.getOutputStream();
				File dest = new File("test.mov");
				// while (!dest.exists()) {
				// dest.createNewFile();
				dest.setReadable(false);
				// }
				while (!dest.canRead()) {
				}
				FileInputStream inputStream = new FileInputStream(dest);

				long totalBytes = 0;
				while (true) {
					byte[] contents = new byte[40960];
					int readBytes = inputStream.read(contents);
					System.err.println("WroteBytes: " + readBytes);
					totalBytes += readBytes;
					if (readBytes == -1) {
						break;
					}
					byte[] realContents = new byte[readBytes];
					System.arraycopy(contents, 0, realContents, 0, readBytes);
					outputStream.write(realContents);
				}

				System.err.println("Total bytes written: " + totalBytes);
				outputStream.close();
				inputStream.close();
			}
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
